FROM node:18-alpine

LABEL maintainer="Siriwan phoksanit <siriwanphoksanit22@gmail.com>"

WORKDIR /home/api

RUN apk update && \
    apk upgrade && \
    apk add --no-cache \
    alpine-sdk git python3 \
    build-base libtool autoconf \
    automake gzip g++ \
    make tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && echo "Asia/Bangkok" > /etc/timezone

# ติดตั้ง pnpm
RUN npm install -g pnpm

COPY . .

RUN pnpm install && pnpm add fastify-plugin && pnpm run build:dist

EXPOSE 8080

CMD ["node", "./dist/server.js"]
