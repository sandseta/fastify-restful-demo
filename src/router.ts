import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';

import indexRoute from './controllers/index'
import demoRouter from './controllers/demo'

import testRouter from './controllers/test';
import schemaRouter from './controllers/schema'
import userRouter from './controllers/user'
import customerRouter from './controllers/customer'
import loginRouter from './controllers/login'
import uploadRouter from './controllers/upload';
import mailRouter from './controllers/mail'

export default async function router(fastify:FastifyInstance) {
    fastify.register(indexRoute,{prefix:'/'})
    fastify.register(demoRouter,{prefix:'/demo'})
   
    fastify.register(schemaRouter, {prefix: '/schema'})
    fastify.register(testRouter, {prefix: '/test'})
    fastify.register(userRouter, {prefix: '/user'})
    fastify.register(customerRouter, {prefix: '/customers'})
    fastify.register(loginRouter, {prefix: '/login'})
    fastify.register(uploadRouter, {prefix: '/uploads'})
    fastify.register(mailRouter,{prefix: 'mail'})

    fastify.addHook('onRoute', (routeOptions) => {
        fastify.log.info(`Route registered: ${routeOptions.method} ${routeOptions.url}`);
      });

      
}


