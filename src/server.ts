import app from "./app";

const port = 8080;
const address = '127.0.0.1';

const start = async () => {
  try {
    await app.listen({ port, host: address });
    console.log(`Server listening at http://${address}:${port}`);

    // เพิ่มการตรวจสอบเส้นทาง
    console.log(app.printRoutes());
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

start();





/*// สร้างอินสแตนซ์ของ Fastify
const server = fastify()*/

/*// กำหนดเส้นทาง (route) พื้นฐาน
server.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.send("Fastify very fast!")
})*/

/*// เริ่มต้นเซิร์ฟเวอร์
server.listen({ port: 8080, host: '127.0.0.1' }, (error, address) => {
    if (error) {
        console.error(error)
        process.exit(1)  // ใช้ 1 เพื่อระบุว่ามีข้อผิดพลาดเกิดขึ้น
    } else {
        console.log('Server listening at ' + address)
    }
})*/
