import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { TestModel } from '../models/test';
import { Knex } from 'knex';

declare module 'fastify' {
  interface FastifyInstance {
    knex: Knex;
  }
}

export default async function test(fastify: FastifyInstance) {
  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: Knex = fastify.knex; // Ensure the knex instance is available

    const testModel = new TestModel();

    try {
      const rs: any = await testModel.test(db);
      reply.send(rs);
    } catch (error) {
      console.error(error);
      reply.code(500).send({ ok: false, error: 'Database query failed' });
    }
  });
}
