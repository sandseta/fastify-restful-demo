import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { UserModel } from '../models/user';
import { Knex } from 'knex';
import * as crypto from 'crypto';

export default async function user(fastify: FastifyInstance) {
  const userModel = new UserModel();

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body;
    const userId = body.userId;
    const username = body.username;
    const password = body.password;
    const firstName = body.firstName;
    const lastName = body.lastName;

    try {
      const encPassword = crypto.createHash('md5').update(password).digest('hex');
      const data: any = {};
      data.user_id = userId;
      data.username = username;
      data.password = encPassword;
      data.first_name = firstName;
      data.last_name = lastName;

      const db: Knex = fastify.knex;
      await userModel.create(db, data);
      reply.send({ ok: true });
    } catch (error) {
      console.error(error);
      reply.code(500).send({ ok: false, error: 'Database query failed' });
    }
  });
}
