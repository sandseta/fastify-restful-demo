import { FastifyInstance,FastifyRequest, FastifyReply } from "fastify";
import { Knex } from 'knex';
import { UserModel } from "../models/user";
import * as crypto from 'crypto';
export default async function login(fastify: FastifyInstance){

    
   const userModel = new UserModel();
   const db: Knex = fastify.db;
  //const db: Knex = fastify.knex;

    fastify.post('/',async (request: FastifyRequest, reply: FastifyReply)=>{
    const body: any = request.body
    const username = body.username
    const password = body.password

    try {
        await db.raw('SELECT 1+1 AS result');
        console.log('Database connection successful');
        
        console.log('Received login request:', { username, password });

        const encPassword = crypto.createHash('md5').update(password).digest('hex')
        console.log('Encrypted password:', encPassword);
        const rs: any= await userModel.login(db,username, encPassword)
        console.log(rs);
        if (rs.length > 0) {
            const user: any = rs[0];
            
            console.log(user);

            const token = fastify.jwt.sign({
                firstName: user.first_name,
                lastName: user.last_name
            })
        
            reply.send({ token })
        } else {
            reply.code(401).send({ok: false, message: 'Login failed!'})
        }
    } catch (error) {
        console.error(error);
        reply.code(500).send({ ok: false, error: 'Database query failed' });

    }


     

    })
}