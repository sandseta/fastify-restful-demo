import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { UserModel } from '../models/user';
import { Knex } from 'knex';
import * as crypto from 'crypto';
import { ok } from 'assert';

export default async function userRoutes(fastify: FastifyInstance) {
  const userModel = new UserModel();
  ///const db: Knex = fastify.knex;
  const db: Knex = fastify.db; // เชื่อมต่อฐานข้อมูล mytest
  
  fastify.post('/',{
    preValidation: [fastify.authenticate]  
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body;
    const username = body.username;
    const password = body.password;
    const firstName = body.firstName;
    const lastName = body.lastName;

    try {
      const encPassword = crypto.createHash('md5').update(password).digest('hex');
      const data: any = {
        username,
        password: encPassword,
        first_name: firstName,
        last_name: lastName
      };

      const db: Knex = fastify.db
      await userModel.create(db, data);
      reply.send({ ok: true });
    } catch (error) {
      console.error(error);
      reply.code(500).send({ ok: false, error: 'Database query failed' });
    }
  });


  fastify.get('/',{
    preValidation: [fastify.authenticate]  
  }, async (request: FastifyRequest, reply: FastifyReply) => {


    try {
      const db: Knex = fastify.db; // ใช้ fastify.db
      const rs: any = await userModel.read(db)
      reply.send(rs);
    } catch (error) {
      console.error(error);
      reply.code(500).send({ ok: false, error: 'Database query failed' });
    }
  });


  //http://localhost:8081/user/search?q=xxxx
  fastify.get('/search',{
    preValidation: [fastify.authenticate]  
  }, async (request: FastifyRequest, reply: FastifyReply) => {
  try {
      const db: Knex = fastify.db; // ใช้ fastify.db
      const query:any =request.query
      const q = query.q
      const rs: any = await userModel.search(db,q)
      reply.send(rs);
    } catch (error) {
      console.error(error);
      reply.code(500).send({ ok: false, error: 'Database query failed' });
    }
  });

  fastify.put('/:userId/edit',{
    preValidation: [fastify.authenticate]  
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body;
    const password = body.password;
    const firstName = body.firstName;
    const lastName = body.lastName;

    const params: any = request.params;
    const userId = params.userId;

    try {
      const db: Knex = fastify.db; // ใช้ fastify.db
      const data: any = { first_name: firstName, last_name: lastName };
      if (password) {
        const encPassword = crypto.createHash('md5').update(password).digest('hex');
        data.password = encPassword;
      }

      await userModel.update(db, userId, data);
      reply.send({ ok: true });
    } catch (error) {
      console.error(error);
      reply.code(500).send({ ok: false, error: 'Database query failed' });
    }
  });


  fastify.delete('/:userId',{
    preValidation: [fastify.authenticate]  
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const db: Knex = fastify.db; // ใช้ fastify.db
      const params: any = request.params;
      const userId = params.userId;
      await userModel.remove(db, userId);
      reply.send({ ok: true });
    } catch (error) {
      console.error(error);
      reply.code(500).send({ ok: false, error: 'Database query failed' });
    }
  });


  fastify.get('/test-mytest', async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const db: Knex = fastify.db;
      const result = await db.raw('SELECT 1+1 AS result');
      reply.send(result);
    } catch (error) {
      console.error(error);
      reply.code(500).send({ ok: false, error: 'Database query failed' });
    }
  });
  
}
