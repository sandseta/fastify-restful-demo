import { FastifyInstance,FastifyRequest, FastifyReply } from "fastify";

//const nodemailer = require("nodemailer")
import * as nodemailer from 'nodemailer'
import * as path from 'path'
import { UserModel } from "../models/user";
//const ejs = require('ejs')
import * as ejs from 'ejs'
import * as fs from 'fs'


export default async function index(fastify: FastifyInstance){

    const userModel = new UserModel()
    const db = fastify.db
    fastify.get('/',async (request: FastifyRequest, reply: FastifyReply)=>{


      try {
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'ulises15@ethereal.email', 
                pass: 'DEHe2TtkScKMPssfYV'
            }
        });

           const sendMain = await transporter.sendMail({
            from: '"siriwan phoksanit" <ulises15@ethereal.email>',
            to: 'demo@demo.com',
            subject: "สวัสดี",
            text: "ทดสอบการส่งเมล",
            html: "สวัสดี <b> ทราย </b>"
        })
    
        reply.send({info: sendMain, url: nodemailer.getTestMessageUrl(sendMain)})
      } catch (error) {
        reply.code(500).send({ok: false, error: 'transporter.sendMain is not a function'})
      }

    })
  

     fastify.get('/template',async (request: FastifyRequest, reply: FastifyReply)=>{


      try {
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'ulises15@ethereal.email', 
                pass: 'DEHe2TtkScKMPssfYV'
            }
        });


        const rs = await userModel.read(db)
        const templateFile = path.join(__dirname, '../../views/mail-template.ejs')
        const html: any = ejs.render(fs.readFileSync(templateFile, 'utf8'), {user: rs})



        const sendMain = await transporter.sendMail({
            from: '"siriwan phoksanit" <ulises15@ethereal.email>',
            to: 'demo@demo.com',
            subject: "สวัสดี",
            text: "ทดสอบการส่งเมล",
            html: html
        })
    
        reply.send({info: sendMain, url: nodemailer.getTestMessageUrl(sendMain)})
      } catch (error) {
        reply.code(500).send({ok: false, error: 'transporter.sendMain is not a function'})
      }

    })


    fastify.get('/attachments',async (request: FastifyRequest, reply: FastifyReply)=>{


      try {
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'ulises15@ethereal.email', 
                pass: 'DEHe2TtkScKMPssfYV'
            }
        });

           const file = path.join(__dirname, '../../public/pdf/demo.pdf')


           const sendMain = await transporter.sendMail({
            from: '"siriwan phoksanit" <ulises15@ethereal.email>',
            to: 'demo@demo.com',
            subject: "ทดสอบส่งไฟล์",
            text: "ส่งไฟล์",
            html: "ส่งไฟล์",
            attachments: [
              {path: file, filename: 'เอกสารสำคัญที่เป็นความลับ.pdf'}
            ]
        })
    
        reply.send({info: sendMain, url: nodemailer.getTestMessageUrl(sendMain)})
      } catch (error) {
        reply.code(500).send({ok: false, error: 'transporter.sendMain is not a function'})
      }

    })
   
  
   
  
}