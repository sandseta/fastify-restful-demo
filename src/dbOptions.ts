// dbOptions.ts

export interface DbOptions {
    client: string;
    connection: {
      host: string;
      port: number;
      user: string;
      password: string;
      database: string;
    };
    connectionName: string; // เพิ่ม property connectionName เข้าไป
    debug: boolean;
  }
  