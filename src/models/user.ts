import { Knex } from 'knex';

export class UserModel {
  async create(db: Knex, data: any) {
    return db('users').insert(data);
  }

  async login(db: Knex, username: any,password: any) {
    return db('users')
    .select('user_id','first_name','last_name')
    .where('username',username)
    .where('password',password);
  }

  async read(db: Knex ){
    return db('users').select('user_id','first_name','last_name').orderBy('first_name')
  }
  
  async search(db: Knex, query: any ){
    const _query = '%' + query +'%'
    return db('users').select('user_id','first_name','last_name')
    .where('first_name','like',_query)
    .orderBy('first_name')
  }

  async update(db: Knex,userId: any, data: any) {
    return db('users')
    .where('user_id',userId)
    .update(data);
  }

  async remove(db: Knex,userId: any) {
    return db('users')
    .where('user_id',userId)
    .del()
  }
}
