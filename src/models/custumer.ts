import { Knex } from 'knex';

export class CustomerModel {
  test(db: Knex) {
    return db('customers').select('*');
  }
}
