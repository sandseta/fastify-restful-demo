import { Knex } from 'knex';

export class FileModel {
  save(db: Knex,file:any) {
    return db('files')
    .insert(file, 'file_id')
  }

  getInfo(db: Knex,fileId:any) {
    return db('files')
    .where('file_id', fileId)
  }



}
