import fastify from 'fastify';
import formbody from '@fastify/formbody';
import cors from '@fastify/cors';

import path from 'path';
const envPath = path.join(__dirname, '../config.conf')
require('dotenv').config({path: envPath})

import fastifyStatic from '@fastify/static';
import fastifyJwt from '@fastify/jwt';
import routes from './router';
import knex from 'knex';
import dbPlugin from './plugins/db';
import {DbOptions} from "./dbOptions";
const multer = require('fastify-multer')
import WebSocket  from 'ws';
import websocketPlugin  from './plugins/ws'; // import ฟังก์ชันจากไฟล์ ws.ts
import ws from './plugins/ws';
import { error } from 'console';
import { Socket } from 'socket.io';
import ioPlugin from './plugins/io'; // import ฟังก์ชันจากไฟล์ io.ts







const app = fastify({
  logger: {
    level: 'info'
  },
  ajv: {
    customOptions: {
      strict: false
    }
  }
});

app.register(multer.contentParser)
app.register(formbody);
app.register(cors);

const dbOptionsMyTest: DbOptions = {
  client: 'mysql2',
  connection: {
    host: process.env.DB1_HOST || 'localhost',
    port: Number(process.env.DB1_PORT)  || 3306,
    user: process.env.DB1_USER || 'root',
    password:  process.env.DB1_PASSWORD ||'',
    database: process.env.DB1_DB1_NAME || 'mytest'
  },
  connectionName: 'db',
  debug: true
};

const dbOptionsMyTest2: DbOptions = {
  client: 'mysql2',
  connection: {
    host: process.env.DB2_HOST || 'localhost',
    port: Number(process.env.DB2_PORT)  || 3306,
    user: process.env.DB2_USER || 'root',
    password:  process.env.DB2_PASSWORD ||'',
    database: process.env.DB2_DB1_NAME || 'mytest2'
  },
  connectionName: 'db2',
  debug: true
};

app.register(require('./plugins/jwt'),{
  secret: process.env.JWT_SECRET || 'Sendy292546'
})

app.register(dbPlugin, dbOptionsMyTest);
app.register(dbPlugin, dbOptionsMyTest2);

app.register(websocketPlugin);




// Register Socket.IO Plugin
app.register(ioPlugin, {});


const clients: any[] = []; // เก็บรายการของทุก client ที่เชื่อมต่ออยู่
app.ready((error: any) => {
  if (error) throw error;
  console.log('Server running...');

  // Ensure that app.ws is defined and properly initialized
  if (app.ws) {
    app.ws.on('connection', (ws: any) => {
      clients.push(ws); // เพิ่ม client ที่เชื่อมต่อเข้ามาในรายการ

      console.log('Client connected!');

      ws.on('message', (message: any) => {
        const receivedMessage = message.toString(); // แปลง Buffer เป็น String
        console.log('Received message:', receivedMessage);

        // ส่งข้อความไปยังทุก client ที่เชื่อมต่ออยู่
        clients.forEach(client => {
          if (client !== ws && client.readyState === ws.OPEN) {
            client.send(receivedMessage);
          }
        });
      });

      ws.on('close', () => {
        // ลบ client ออกจากรายการเมื่อ disconnect
        clients.splice(clients.indexOf(ws), 1);
      });
    });
  } else {
    console.error('app.ws is not defined.');
  }


});



app.register(require('@fastify/static'), {
  root: path.join(__dirname, '../public'),
  prefix: '/assets/', // optional: default '/'
});

app.register(require('@fastify/view'), {
  engine: {
    ejs: require('ejs')
    
  },
  root: path.join(__dirname, '../views'),
  includeViewExtension: true
});

app.register(routes); // ลงทะเบียนเส้นทางทั้งหมด
//app.register(require('./plugins/ws'))
export default app;  // ใช้ export default
