import fp from 'fastify-plugin';
import { FastifyInstance, FastifyPluginAsync } from 'fastify';
import { Server as WebSocketServer } from 'ws';

const websocketPlugin: FastifyPluginAsync = async (fastify: FastifyInstance, opts: any) => {
  const wsOptions: any = {
    server: fastify.server,
    path: '/ws'
  };

  const wss = new WebSocketServer(wsOptions);

  // เพิ่ม event listener เมื่อมีการเชื่อมต่อ WebSocket
  wss.on('connection', (ws) => {
    console.log('Client connected');
    // เพิ่มเหตุการณ์เมื่อมีข้อมูลเข้ามาจาก client
    ws.on('message', (message) => {
      console.log('Received message:', message);
    });
    // เพิ่มเหตุการณ์เมื่อเชื่อมต่อถูกปิด
    ws.on('close', () => {
      console.log('Client disconnected');
    });
  });

  // เพิ่มคุณลักษณะของ WebSocket server เข้าไปใน Fastify instance
  fastify.decorate('ws', wss);

  // เพิ่ม hook เมื่อ Fastify instance ปิด
  fastify.addHook('onClose', (instance, done) => {
    wss.close(() => {
      console.log('WebSocket server closed');
      done();
    });
  });
};

// ใช้ fastify-plugin เพื่อสร้าง plugin
export default fp(websocketPlugin, { name: 'fastify-websocket-plugin' });
