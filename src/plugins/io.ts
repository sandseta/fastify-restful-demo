import { FastifyPluginCallback } from 'fastify';
import fastifySocketIO from 'fastify-socket.io';
import { Server, Socket } from 'socket.io';

const ioPlugin: FastifyPluginCallback = (fastify, options, done) => {
  fastify.register(fastifySocketIO, {
    cors: {
      origin: '*',
    },
  });

  fastify.ready((err) => {
    if (err) throw err;

    const io: Server = fastify.io;

    io.on('connection', (socket: Socket) => {
      console.log('Client connected');

      socket.on('message', (message: string) => {
        console.log('Received message:', message);
        // Broadcast message to all connected clients
        io.emit('message', message);
      });

      socket.on('disconnect', () => {
        console.log('Client disconnected');
      });
    });
  });

  done();
};

export default ioPlugin;
