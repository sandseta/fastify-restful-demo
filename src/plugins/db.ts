import fp from 'fastify-plugin';
import { FastifyPluginAsync } from 'fastify';

import knex, { Knex } from 'knex';
import { FastifyInstance } from 'fastify';
import {DbOptions} from "src/dbOptions";// ตรวจสอบที่อยู่ของไฟล์ที่ถูกต้อง

const dbPlugin: FastifyPluginAsync<DbOptions> = async (fastify, opts) => { // ใช้ DbOptions กับ FastifyPluginAsync
    try {
      const connection: Knex = knex({
        client: opts.client,
        connection: opts.connection,
        debug: opts.debug
      });
      await connection.raw('SELECT 1+1 AS result'); // ทดสอบการเชื่อมต่อ
      fastify.decorate(opts.connectionName, connection);
    } catch (error) {
      throw new Error('Failed to connect to the database');
    }
  };
  
  export default fp(dbPlugin, {
    name: 'dbPlugin' // ระบุชื่อของ plugin สำหรับ fastify-plugin
  });