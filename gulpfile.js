const gulp = require('gulp');
const clean = require('gulp-clean');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const pump = require('pump'); // ใช้ pump แทน pipeline

const tsProject = ts.createProject('tsconfig.json');

gulp.task('compile', function () {
    const tsResult = tsProject.src()
        .pipe(sourcemaps.init())
        .pipe(tsProject());

    return tsResult.js
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./tmp/dist'));
});

gulp.task('uglify', function (cb) { // เพิ่ม callback function เพื่อ handle errors
    pump([
        gulp.src('./tmp/dist/**/*.js'),
        uglify(),
        gulp.dest('dist')
    ], cb);
});

gulp.task('clean', function () {
    return gulp.src('./tmp', { read: false })
        .pipe(clean());
});

gulp.task('default', gulp.series('compile', 'uglify', 'clean'));
