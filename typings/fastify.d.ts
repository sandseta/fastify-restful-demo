import * as knex from 'knex';

declare module 'fastify' {
    interface FastifyInstance {
        knex: knex.Knex  // ใช้ knex.Knex เพื่อระบุประเภทของ knex
        db: knex.Knex
        db2: knex.Knex
        jwt:any
        authenticate: any
        ws: WebSocketServer
        ws:any
        io:any
    }
    interface FastifyRequest{
        jwtVerify: any
        file: any
        files: any[]
    }

    interface FastifyReply{
        view: any
    }
}
